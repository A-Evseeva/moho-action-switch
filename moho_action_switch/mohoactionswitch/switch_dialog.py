import sys, glob, ntpath, re
from PyQt5.QtWidgets import (QApplication, QDialog,
                             QPushButton, QLabel, QVBoxLayout, QSizePolicy, 
                             QScrollArea, QWidget, QMainWindow,
                             QGridLayout, QTabWidget)
from PyQt5.QtGui import (QIcon)
from PyQt5.QtCore import (QSize, Qt)


class SwitchDialog(QMainWindow):
    """
    Simple dialog that consists of a Progress Bar and a Button.
    Clicking on the button results in the start of a timer and
    updates the progress bar.
    """
    def __init__(self):
        super().__init__()
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle('Switch')
        self.width = 600
        self.height = 600
        self.resize(self.width, self.height)
        self.columns = 3


        self.layout = QVBoxLayout()
        self.widget = QTabWidget()
        self.widget.resize(self.width, self.height)

        self.iconPath = sys.argv[1]  
        for i in range(2, len(sys.argv)):
            nextArg = sys.argv[i]
            argParts = nextArg.split('+')
            nextName = argParts[0]
            filteredFrames = [int(fr) for fr in argParts[1:]]
            nextTab = QWidget()
            

            nextTab.layout = QGridLayout()
            nextTab.scroll = QScrollArea()  

            buttons = []
            for filename in glob.glob(self.iconPath + nextName + self.iconPath[-1:] + "*.png"):
                name = ntpath.basename(filename)
                splitname = re.match(r'\D*(\d+)\.(.*)\.png', name)
                if int(splitname.group(1)) in filteredFrames:
                    nextButton = QPushButton(splitname.group(2), self)
                    nextButton.actNo = int(splitname.group(1))
                    nextButton.actName = nextName
                    nextButton.setIcon(QIcon(filename))
                    nextButton.setIconSize(QSize(100,100))
                    nextButton.clicked.connect(self.whichbtn)
                    nextTab.layout.addWidget(nextButton, len(buttons)/self.columns, len(buttons)%self.columns)
                    buttons.append(nextButton)

            nextTab.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
            nextTab.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            nextTab.scroll.setWidgetResizable(True)
            nextTab.scroll.setWidget(nextTab) 

            nextTab.setLayout(nextTab.layout)
            self.widget.addTab(nextTab.scroll, nextName)
       
        self.widget.setLayout(self.layout)
        
        self.setCentralWidget(self.widget)
        self.show()
       
    def whichbtn(self, no):
        print('{0}+{1}'.format(self.sender().actName, self.sender().actNo))
        sys.exit()


def run_switch_app():
    app = QApplication(sys.argv)
    window = SwitchDialog()
    sys.exit(app.exec_())

if __name__ == '__main__': run_switch_app()