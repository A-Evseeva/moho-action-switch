from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='mohoactionswitch',
    version="1.0",
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    include_package_data=True,
    install_requires=[
        'pyqt5>=5.15.2',
    ],
    entry_points={
    'console_scripts':
        ['mohoactionswitch = mohoactionswitch.switch_dialog:run_switch_app']
    }

)