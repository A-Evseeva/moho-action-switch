-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "AE_action_switch"

-- **************************************************
-- General information about this script
-- **************************************************

AE_action_switch = {}

function AE_action_switch:Name()
	return 'Action Switch'
end

function AE_action_switch:Version()
	return '1.0'
end

function AE_action_switch:UILabel()
	return 'Action Switch'
end

function AE_action_switch:Creator()
	return 'Alexandra Evseeva'
end

function AE_action_switch:Description()
	return ''
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function AE_action_switch:IsRelevant(moho)
	return true
end

function AE_action_switch:IsEnabled(moho)
	return true
end

-- **************************************************
-- AE_action_switchDialog
-- **************************************************
--[[
local AE_action_switchDialog = {}

function AE_action_switchDialog:new()
	local d = LM.GUI.SimpleDialog('Action Switch', AE_action_switchDialog)
	local l = d:GetLayout()

	return d
end
--]]
-- **************************************************
-- The guts of this script
-- **************************************************

function AE_action_switch:Run(moho)
	--[[
	local dlog = AE_action_switchDialog:new(moho)
	if (dlog:DoModal() == LM.GUI.MSG_CANCEL) then
		return
	end
	--]]
	moho.document:SetDirty()
	moho.document:PrepUndo(nil)
	
	-- Your code here:
	local args = {}
	local persFolder = self:GetPersFolder(moho)
	if not persFolder then 
		return LM.GUI.Alert(
			LM.GUI.ALERT_WARNING,
			"Character folder not found")
	end
	local interp = MOHO.InterpSetting:new_local()
	for a = 0, moho.layer:CountActions()-1 do 
		local actionName = moho.layer:ActionName(a)
		local markerChannel = moho.layer.fTimelineMarkers:ActionByName(actionName)
		if markerChannel then
			filteredFrames = {}
			for k=1, markerChannel:CountKeys()-1 do
				table.insert(filteredFrames, markerChannel:GetKeyWhen(k))
			end
			if self:exists(persFolder.."/"..actionName.."/") then
				for i,channel in AE_Utilities:IterateLayerSubChannels(moho, moho.layer) do
					local actChannel = channel:ActionByName(actionName)
					local isFiltering = false
					local symFilter = false
					if actChannel then 
						for k=1, actChannel:CountKeys()-1 do
							actChannel:GetKeyInterpByID(k,interp)
							if interp.tags == 1 then
								isFiltering = true
								symFilter = true
								break
							elseif interp.tags == 2 then
								isFiltering = true
								break								
							end
						end
						if isFiltering then
							local derived = AE_Utilities:GetDerivedChannel(moho, channel)
							local currentVal = derived:GetValue(moho.frame)
							local zeroVal = derived:GetValue(0)
							if symFilter then currentVal = math.abs(currentVal - zeroVal) end
							local actDerived = AE_Utilities:GetDerivedChannel(moho, actChannel)
							j =1
							while j <= #filteredFrames do
								actVal = actDerived:GetValue(filteredFrames[j])
								if symFilter then actVal = math.abs(actVal - zeroVal) end
								if not AE_Utilities:IsEqualValues(actDerived, currentVal, actVal, 0.0001) then
									table.remove(filteredFrames, j)
									j = j - 1
								end
								j = j + 1
							end
						end
					end
				end
			end
			argString = actionName
			for i,v in pairs(filteredFrames) do 
				argString = argString .. "+" .. v
			end
			table.insert(args, argString)
		end
	end


	local cmd = "mohoactionswitch"
	--local cmd = "python D:/_coding/moho-action-switch/moho_action_switch/mohoactionswitch/switch_dialog.py" --for debug purpoces
	--local cmd = "start pythonw.exe -m mohoactionswitch.switch_dialog" --works but does not return prints
	--local cmd = "set IS_MINIMIZED=1 && start /min mohoactionswitch" --works but does not return prints
	--local cmd = "powershell -window minimized -command \"mohoactionswitch\"" -- works!
	
	
	--local arg = "P:/2020zver/pers/badgercat/moho/lipsync/*.png"
	local arg = persFolder
	local actions = ""
	for i,v in pairs(args) do actions = actions .. " " .. v end	
	
	
	local slash = package.config:sub(1,1)
	if string.sub(arg, -1) ~= "/" then arg = arg .. "/" end
	if slash == "\\" then
		arg = string.gsub(arg, "/", slash)
	end

	local cmdfull = string.format("%s '%s' %s", cmd, arg, actions)

	if slash == "\\" then
		arg = string.gsub(arg, "/", slash)
		cmdfull = string.format('powershell -window minimized -command "%s"', cmdfull)
	end
	
	print(cmdfull)


	local f = assert(io.popen(cmdfull, 'r'))
	local s = assert(f:read('*a'))
	f:close()
	
	print(s)
	local spl = string.find(s, "+")
	if spl then
		local action = string.sub(s, 1, spl-1)
		local frame = tonumber(string.sub(s, spl+1))
		for i, layer in AE_Utilities:IterateAllLayers(moho) do
			if layer == moho.layer or layer:IsAncestorSelected() then
				for j, channel in AE_Utilities:IterateLayerSubChannels(moho, layer) do
					local actChannel = channel:ActionByName(action)
					if actChannel and actChannel:Duration()>0 then
						local isFiltering = false
						for k=1, actChannel:CountKeys()-1 do
							actChannel:GetKeyInterpByID(k,interp)
							if interp.tags == 1 or interp.tags == 2 then
								isFiltering = true
								break
							end
						end
						if not isFiltering then
							local derived = AE_Utilities:GetDerivedChannel(moho, channel)
							local actDerived = AE_Utilities:GetDerivedChannel(moho, actChannel)
							derived:SetValue(moho.frame, actDerived:GetValue(frame))
						end
					end
				end
				layer:UpdateCurFrame()
			end
		end
	end

end

function AE_action_switch:FindPersLayer(moho)
	local layer = moho.layer
	local parents = {}
	while layer do
		table.insert(parents, layer)
		layer = layer:Parent()
	end
	for i=#parents, 1, -1 do
		if moho:LayerAsBone(parents[i]) and parents[i]:Name()==string.upper(parents[i]:Name()) then
			return parents[i]
		end
	end 
end

function AE_action_switch:GetPersFolderFromProjectPath(moho, persPath)
	local t={}
	local ownPath = moho.document:Path()
	for str in string.gmatch(persPath, "([^/]+)") do
			table.insert(t, str)
	end
	for i,v in pairs(t) do
		st, en = string.find(ownPath, v) 
		if st then 
			st1, en1 = string.find(persPath, v)
			persPath = string.sub(ownPath, 1, en) .. string.sub(persPath, en1 + 1)
			return persPath
		end
	end
end

function AE_action_switch:GetPersFolder(moho)
	local layer = self:FindPersLayer(moho)
	local scriptData = layer:ScriptData()
	local persPath = scriptData:GetString('perspath')
	if self:exists(persPath.."/") then return persPath end

	if persPath then
		persPath = self:GetPersFolderFromProjectPath(moho, persPath)
	end
	if not persPath or not self:exists(persPath.."/") then 
		local question = string.format("Select folder where source %s character is saved", layer:Name())
		persPath = LM.GUI.SelectFolder(question)
	end
	if persPath then 
		scriptData:Set('perspath', persPath)
		return persPath
	end
	return nil
end

function AE_action_switch:exists(file)
	local ok, err, code = os.rename(file, file)
	if not ok then
	   if code == 13 then
		  -- Permission denied, but it exists
		  return true
	   end
	end
	return ok, err
 end


