-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "AE_ActionSwitchSettings"

-- **************************************************
-- General information about this script
-- **************************************************

AE_ActionSwitchSettings = {}

function AE_ActionSwitchSettings:Name()
	return 'Action Switch Settings'
end

function AE_ActionSwitchSettings:Version()
	return '1.0'
end

function AE_ActionSwitchSettings:UILabel()
	return 'Action Switch Settings'
end

function AE_ActionSwitchSettings:Creator()
	return 'Alexandra Evseeva'
end

function AE_ActionSwitchSettings:Description()
	return 'Render icons for an action (for using with AE_ActionSwitch)'
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function AE_ActionSwitchSettings:IsRelevant(moho)
	return (moho.document:CurrentDocAction() ~= "")
end

function AE_ActionSwitchSettings:IsEnabled(moho)
	return (moho.document:CurrentDocAction() ~= "")
end

-- **************************************************
-- Recurring Values
-- **************************************************

AE_ActionSwitchSettings.from = 1
AE_ActionSwitchSettings.to = -1
AE_ActionSwitchSettings.drawMode = false
AE_ActionSwitchSettings.boxStart = LM.Vector2:new_local()
AE_ActionSwitchSettings.boxFinish = LM.Vector2:new_local()
AE_ActionSwitchSettings.drawRect = LM.Rect:new_local()

-- **************************************************
-- Prefs
-- **************************************************

function AE_ActionSwitchSettings:LoadPrefs(prefs)
	self.from = prefs:GetInt("AE_ActionSwitchSettings.from", 1)
	self.to = prefs:GetInt("AE_ActionSwitchSettings.to", -1)
end

function AE_ActionSwitchSettings:SavePrefs(prefs)
	prefs:SetInt("AE_ActionSwitchSettings.from", self.from)
	prefs:SetInt("AE_ActionSwitchSettings.to", self.to)
end

function AE_ActionSwitchSettings:ResetPrefs()
	self.from = 1
	self.to = -1
end

-- **************************************************
-- Keyboard/Mouse Control
-- **************************************************

function AE_ActionSwitchSettings:OnMouseDown(moho, mouseEvent)
	if self.drawMode then
		self.boxStart:Set(mouseEvent.startVec)
	end
end

function AE_ActionSwitchSettings:OnMouseMoved(moho, mouseEvent)
	if self.drawMode then
		self.drawRect:Set(mouseEvent.startPt.x, mouseEvent.startPt.y, mouseEvent.pt.x, mouseEvent.pt.y)
		mouseEvent.view:DrawMe()
	end
end

function AE_ActionSwitchSettings:OnMouseUp(moho, mouseEvent)
	if self.drawMode then
		self.drawMode = false
		self.boxFinish:Set(mouseEvent.vec)
		self:EvalRender(moho)
	end
end

function AE_ActionSwitchSettings:OnKeyDown(moho, keyEvent)
	
end

function AE_ActionSwitchSettings:DrawMe(moho, view)
	--[[
	local mesh = moho:DrawingMesh()
	if (mesh == nil) then
		return
	end
	--]]

	local g = view:Graphics()
	local matrix = LM.Matrix:new_local()

	moho.drawingLayer:GetFullTransform(moho.frame, matrix, moho.document)
	g:Push()
	g:ApplyMatrix(matrix)
	
	if self.drawMode then
		g:SelectionRect(self.drawRect)
	end 
end

-- **************************************************
-- Tool Panel Layout
-- **************************************************

AE_ActionSwitchSettings.RENDER = MOHO.MSG_BASE
AE_ActionSwitchSettings.FROM = MOHO.MSG_BASE + 1
AE_ActionSwitchSettings.TO = MOHO.MSG_BASE + 2

function AE_ActionSwitchSettings:DoLayout(moho, layout)
	self.RenderButton = LM.GUI.Button('RENDER', self.RENDER)
	layout:AddChild(self.RenderButton, LM.GUI.ALIGN_LEFT, 0)

	self.fromInput = LM.GUI.TextControl(0, "1", self.FROM, LM.GUI.FIELD_INT, 'From:')
	layout:AddChild(self.fromInput, LM.GUI.ALIGN_LEFT, 0)

	self.toInput = LM.GUI.TextControl(0, "-1", self.TO, LM.GUI.FIELD_INT, 'to:')
	layout:AddChild(self.toInput, LM.GUI.ALIGN_LEFT, 0)
end

function AE_ActionSwitchSettings:UpdateWidgets(moho)
	if self.to <= 0 then self.to = moho.document:AnimDuration() end
	if self.from <= 0 then self.from = 1 end

	AE_ActionSwitchSettings.fromInput:SetValue(self.from)
	AE_ActionSwitchSettings.toInput:SetValue(self.to)
end

function AE_ActionSwitchSettings:HandleMessage(moho, view, msg)
	if msg == self.RENDER then
		if not self.drawMode then 
			self.drawMode = true
		else 
			self.drawMode = false
			self:EvalRender(moho)
		end
	elseif msg == self.FROM then
		self.from = self.toInput:IntValue()
	elseif msg == self.TO then
		self.to = self.toInput:IntValue()
	else
		
	end
end

function AE_ActionSwitchSettings:GetRenderFolder(moho)
	local persLayer = AE_action_switch:FindPersLayer(moho)
	local scriptData = persLayer:ScriptData()
	if scriptData:HasKey('perspath') then return scriptData:GetString('perspath') end
	local ownPath = string.gsub(moho.document:Path(), "\\", "/")
	local persPath = string.gsub(ownPath, "/[^/]+$", "")
	scriptData:Set('perspath', persPath)
	return persPath
end

function AE_ActionSwitchSettings:EvalRender(moho)
	-- render frames
	local camDimensions = (self.boxFinish - self.boxStart)
	local camCenter = self.boxStart + camDimensions/2
	local camSize = math.max(math.abs(camDimensions.x), math.abs(camDimensions.y))
	local renderSize = 300
	
	local saveFolder = self:GetRenderFolder(moho) .. "/" .. moho.document:CurrentDocAction() .. "/"
	if not AE_file_exists(saveFolder) then 
		local createCommand = string.format('mkdir "%s"', string.sub(saveFolder, 1, -2))
		print(createCommand)
		os.execute(createCommand)
	end
	
	local width = moho.document:Width()
	local height = moho.document:Height()
	local posMuted = moho.document.fCameraTrack:IsMuted()
	moho.document.fCameraTrack:Mute(false)
	local zoomMuted = moho.document.fCameraZoom:IsMuted()
	moho.document.fCameraZoom:Mute(false)	
	local oldCameraPos = moho.document.fCameraTrack:GetValue(0)
	local newCameraPos = LM.Vector3:new_local()
	newCameraPos:Set(camCenter.x, camCenter.y, oldCameraPos.z)
	local zoom = moho.document.fCameraZoom:GetValue(0)
	moho.document:SetShape(renderSize, renderSize)
	moho.document.fCameraTrack:SetValue(0, newCameraPos)
	local camAngle = 2.0 * math.atan(camSize/(newCameraPos.z * 2))
	local newZoom = 60 / (180.0 * camAngle/math.pi)
	moho.document.fCameraZoom:SetValue(0, newZoom)
	
	local markerChannel = moho.layer.fTimelineMarkers
	for f=self.from, self.to do
		if markerChannel:HasKey(f) then 
			local markerName = markerChannel:GetValue(f)
			local fileName = saveFolder .. string.format("_%05d.%s.png", f, markerName)
			moho:SetCurFrame(f)
			print("rendering ", fileName)
			moho:FileRender(fileName)
		end
	end

	moho.document.fCameraTrack:SetValue(0, oldCameraPos)
	moho.document.fCameraZoom:SetValue(0, zoom)
	moho.document:SetShape(width, height)
	moho.document.fCameraTrack:Mute(posMuted)
	moho.document.fCameraZoom:Mute(zoomMuted)	
end
